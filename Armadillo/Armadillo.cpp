// Armadillo.cpp : Defines the exported functions for the DLL application.
//

#include "Armadillo.hpp"
#include <iostream>

void Armadillo::cry()
{
	std::cout << "This is an Armadillo cry" << std::endl;
}

Armadillo::Armadillo()
{
}


Armadillo::~Armadillo()
{
}


extern "C"
{
#ifdef _WIN32
	__declspec(dllexport)
#endif
		IAnimal *getInstance()
	{
		return new Armadillo();
	}
}