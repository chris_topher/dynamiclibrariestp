#pragma once
#include "IAnimal.hpp"

class Armadillo :
	public IAnimal
{
public:
	Armadillo();
	virtual ~Armadillo();
	void cry();
};

