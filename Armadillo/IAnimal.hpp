#pragma once
#include "IAnimal.hpp"

class IAnimal
{
public:
	virtual void cry() = 0;
	IAnimal() {}
	virtual ~IAnimal() {}
};