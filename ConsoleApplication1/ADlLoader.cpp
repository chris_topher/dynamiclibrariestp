#ifdef _WIN32
	#include "DlLoader.hpp"
#else
	#include "UnixDlLoader.hpp"
#endif

ADlLoader *ADlLoader::getDefaultLoader(const std::string &path)
{
#ifdef _WIN32
	return new DlLoader(path);
#else
	return new UnixDlLoader(path);
#endif
}
