#pragma once

#include <string>
#include <stdexcept>
#include <iostream>
#include "Mob.hpp"

class ADlLoader
{
public:
	typedef Mob *(*InstancePtr)();
	ADlLoader(){}
	virtual ~ADlLoader() { }
	virtual Mob *getInstance() = 0;
	static ADlLoader *getDefaultLoader(const std::string &path);

};

