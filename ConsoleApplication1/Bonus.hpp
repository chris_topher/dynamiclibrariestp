#pragma once

#include <random>
#include "AGameObject.hpp"

enum bonusType
{
	Life = 0,
	NBR_BONUS
};

class Bonus : public AGameObject
{
public:
	Bonus();
	virtual ~Bonus();

	virtual void init(int ID, int x, int y, int sizeX, int sizeY, int speedX, int speedY);

private:
	bonusType	_typeOfBonus;
};
