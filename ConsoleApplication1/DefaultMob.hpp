#pragma once
#include "Mob.hpp"
#include <iostream>

class DefaultMob : Mob
{
public:
	DefaultMob();
	virtual ~DefaultMob();
	virtual void init(int id, int x, int y);
	virtual void toString();
};
