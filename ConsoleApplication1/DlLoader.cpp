#define _CRT_SECURE_NO_DEPRECATE
#include "DlLoader.hpp"



DlLoader::DlLoader()
{
}


DlLoader::DlLoader(const std::string & path)
{
	_dllPath = new wchar_t[path.size() + 1];
	mbstowcs(_dllPath, path.c_str(), path.size() + 1);
	this->_dllHandle = LoadLibrary(_dllPath);
	if (_dllHandle == NULL)
		throw std::runtime_error("DlLoader::(constructor) : error: Cannot load library.");
	_instPtr = reinterpret_cast<InstancePtr>(GetProcAddress(_dllHandle, "getInstance"));
	if (_instPtr == NULL)
		throw std::runtime_error("DlLoader::(constructor) : error: Cannot get InstancePtr");
}

DlLoader::~DlLoader()
{
	delete _dllPath;
	if (FreeLibrary(_dllHandle) == 0)
		std::cerr << ("DlLoader::(destructor) : error: Cannot free library.") << std::endl;
}

Mob *DlLoader::getInstance()
{
	return _instPtr();
}
