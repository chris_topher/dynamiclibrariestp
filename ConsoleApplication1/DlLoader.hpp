#pragma once

#include <Windows.h>
#include <tchar.h>
#include <string>
#include <cstring>
#include <stdlib.h>
#include <iostream>
#include "ADlLoader.hpp"
#include "Mob.hpp"

class DlLoader : public ADlLoader
{
protected:
	wchar_t  *_dllPath;
	HINSTANCE _dllHandle;
	InstancePtr _instPtr;
	DlLoader();
public:
	DlLoader(const std::string &str);
	virtual ~DlLoader();
	virtual Mob *getInstance();
};

