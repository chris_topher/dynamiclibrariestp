#include "MonsterGuardian.hpp"
#include <algorithm>
#include <stdlib.h>
#include <iostream>

void MonsterGuardian::addMobObject(const std::string &typeNameToAdd)
{
	std::string typeName = typeNameToAdd;
	std::transform(typeName.begin(), typeName.end(), typeName.begin(), ::tolower);
	if (_loaders.find(typeName) != _loaders.end())
		std::cerr << "No need to add object " << typeName << " because this type is already in stack.";
	else
	{
		try
		{
			_loaders[typeName] = ADlLoader::getDefaultLoader(typeNameToAdd + defaultLibrarySuffix);
		}
		catch (std::runtime_error &e)
		{
			std::cout << e.what() << std::endl;
		}
	}
}

Mob *MonsterGuardian::getMobObjectByTypeName(const std::string &nameToSeek) const
{
	std::string name = nameToSeek;
	std::transform(name.begin(), name.end(), name.begin(), ::tolower);
	if (_loaders.find(name) != _loaders.end())
		return (_loaders.at(name)->getInstance());
	else
		std::cerr << "MonsterGuardian::getMobObjectByTypeName : Did not find any object '" + name + "' in stack." << std::endl;
	return (NULL);
}

MonsterGuardian::MonsterGuardian()
{
}


MonsterGuardian::~MonsterGuardian()
{
	for (std::pair<std::string, ADlLoader *> x : _loaders)
	{
		delete x.second;
	}
}
