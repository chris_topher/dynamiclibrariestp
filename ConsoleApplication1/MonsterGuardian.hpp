#pragma once
#include <unordered_map>

#include "ADlLoader.hpp"
#include <string>
#include "Mob.hpp"

class MonsterGuardian
{
protected:
#ifdef _WIN32
	const std::string defaultLibrarySuffix = ".dll";
#else
	const std::string defaultLibrarySuffix = ".so";
#endif
	std::unordered_map<std::string, ADlLoader *> _loaders;
public:
	Mob *getMobObjectByTypeName(const std::string &typeName) const;
	void	addMobObject(const std::string &typeName);
	MonsterGuardian();
	~MonsterGuardian();
};

