#include "UnixDlLoader.hpp"



UnixDlLoader::UnixDlLoader()
{
}


UnixDlLoader::UnixDlLoader(const std::string & path)
{
	this->_path = ("./lib" + path).c_str();
	this->_handler = dlopen(_path, RTLD_LAZY);
	if (_handler == NULL)
		throw std::runtime_error("UnixDlLoader::(constructor) : error while loading library\n" + std::string(dlerror()));
	this->_inst = reinterpret_cast<InstancePtr>(dlsym(_handler, "getInstance"));
	if (_inst == NULL)
		throw std::runtime_error("UnixDlLoader::(constructor) : error while loading library\n" + std::string(dlerror()));
}

UnixDlLoader::~UnixDlLoader()
{
	dlclose(_handler);
}

Mob * UnixDlLoader::getInstance()
{
	return _inst();
}
