#pragma once
#include "ADlLoader.hpp"
#include <string>
#include <dlfcn.h>

class UnixDlLoader :
	public ADlLoader
{
protected:
	const char *_path;
	void *_handler;
	InstancePtr _inst;
	UnixDlLoader();
public:
	UnixDlLoader(const std::string &str);
	virtual ~UnixDlLoader();
	virtual Mob *getInstance();
};

