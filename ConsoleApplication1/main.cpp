// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "MonsterGuardian.hpp"
#include <iostream>

void to()
{

	MonsterGuardian d;

	try
	{
		d.addMobObject("DefaultMob");
		Mob *mob = reinterpret_cast<Mob *>(d.getMobObjectByTypeName("defAuLtMob"));
		mob->init(1, 2, 3);
		std::cout << mob->getSize().first<< " && " << mob->getSize().second;
	}
	catch (const std::exception &e)
	{
		std::cerr << e.what() << std::endl;
	}
}

int main()
{
	to();
}