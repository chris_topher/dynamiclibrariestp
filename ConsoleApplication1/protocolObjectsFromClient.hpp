#pragma once
#include <cstdint>

//FromClient objects
//Objects sent by client

//Objects sent by UDP:
struct Move
{
  int32_t direction;
};

struct Shoot
{
  //Content Unknown for now
};


//Objects sent by TCP:
struct Login
{
  char username[256];
  char ip[16];
};

struct Logout
{
  char username[256];
  char ip[16];
};

struct NewRoom
{
  char name[256];
  int32_t gameType;
};

struct LaunchGameSession
{
  char roomName[256];
};

struct RoomQuit
{
  char roomName[256];
};

struct RoomSelect
{
  char roomName[256];
};
