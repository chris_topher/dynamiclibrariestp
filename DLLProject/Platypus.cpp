
#include "Platypus.hpp"
#include <iostream>

Platypus::Platypus()
{
}


Platypus::~Platypus()
{
}

void Platypus::cry()
{
	std::cout << "this is a Platypus cry !!!" << std::endl;
}


extern "C"
{
	#ifdef _WIN32
	__declspec(dllexport)
	#endif
	IAnimal *getInstance()
	{
		return new Platypus();
	}
}