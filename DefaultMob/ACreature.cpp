#include "ACreature.hpp"

ACreature::ACreature()
{}

ACreature::~ACreature()
{}

void	ACreature::init(int ID, ObjectType type, int x, int y, int sizeX, int sizeY, int speedX, int speedY, Bullet bullet, int pv)
{
	AGameObject::init(ID, type, x, y, sizeX, sizeY, speedX, speedY);
	_pv = pv;
	_bullet = bullet;
}

void	ACreature::shoot()
{}