#pragma once

#include <vector>
#include "AGameObject.hpp"
#include "Bullet.hpp"

class ACreature : public AGameObject
{
public:
	ACreature();
	virtual	~ACreature();

	virtual void	init(int ID, ObjectType type, int x, int y, int sizeX, int sizeY, int speedX, int speedY, Bullet bullet, int pv);

	virtual void	shoot();

protected:
	int									_pv;
	Bullet							_bullet;
	std::vector<Bullet>	_bullets;
};
