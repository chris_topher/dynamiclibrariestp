#include "AGameObject.hpp"

AGameObject::AGameObject()
{}

AGameObject::~AGameObject()
{}

void	AGameObject::init(int ID, ObjectType type, int x, int y, int sizeX, int sizeY, int speedX, int speedY)
{
	_ID = ID;
	_type = type;
	_coord.first = x;
	_coord.second = y;
	_size.first = sizeX;
	_size.second = sizeY;
	_speed.first = speedX;
	_speed.second = speedY;
}

bool	AGameObject::checkHitBox(AGameObject &other)
{
	std::pair<int, int>	coord2 = other.getCoord();
	std::pair<int, int>	size2 = other.getSize();

	if ((coord2.first >= _coord.first + _size.first)			// right
		|| (coord2.first + size2.first <= _coord.first)			// left
		|| (coord2.second >= _coord.second + _size.second)	// down
		|| (coord2.second + size2.second <= _coord.second))	// up
		return (false);
	else
		return (true);
}

void	AGameObject::move()
{
	_coord.first += _speed.first;
	_coord.second += _speed.second;
}

void	AGameObject::destroy()
{
	this->~AGameObject();
}

std::pair<int, int>	AGameObject::getCoord() const
{
	return (_coord);
}

std::pair<int, int>	AGameObject::getSize() const
{
	return (_size);
}