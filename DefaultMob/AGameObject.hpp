#pragma once

#include <utility>
#include "protocolHeader.hpp"

class AGameObject
{
public:
	AGameObject();
	virtual ~AGameObject();

	virtual void	init(int ID, ObjectType type, int x, int y, int sizeX, int sizeY, int speedX, int speedY);

	virtual bool	checkHitBox(AGameObject &other);
	virtual void	move();
	virtual void	destroy();

	virtual	std::pair<int, int>	getCoord() const;
	virtual	std::pair<int, int>	getSize() const;

protected:
	int									_ID;
	ObjectType					_type;
	std::pair<int, int>	_coord;
	std::pair<int, int>	_size;
	std::pair<int, int>	_speed;
};