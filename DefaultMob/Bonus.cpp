#include "Bonus.hpp"

Bonus::Bonus()
{}

Bonus::~Bonus()
{}

void Bonus::init(int ID, int x, int y, int sizeX, int sizeY, int speedX, int speedY)
{
	AGameObject::init(ID, BONUS, x, y, sizeX, sizeY, 3, 3); // vitesse purement arbitraire

	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(0, NBR_BONUS - 1);

	_typeOfBonus = (bonusType)distribution(generator);
}