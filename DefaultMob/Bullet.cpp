#include "GameObjects\Bullet.hpp"

Bullet::Bullet()
{}

Bullet::~Bullet()
{}

void	Bullet::init(int ID, int x, int y, int sizeX, int sizeY, int speedX, int speedY, int underType, int dmg, int penetration)
{
	AGameObject::init(ID, BULLET, x, y, sizeX, sizeY, speedX, speedY);
	_underType = underType;
	_damage = dmg;
	_penetration = penetration;
}