#pragma once

#include "AGameObject.hpp"

class Bullet : public AGameObject
{
public:
	Bullet() {}
	virtual ~Bullet() {}

	virtual void	init(int ID, int x, int y, int sizeX, int sizeY, int speedX, int speedY, int underType, int dmg, int penetration) {}

private:
	int	_underType; // faire un enum ou autre
	int	_damage;
	int	_penetration;
};
