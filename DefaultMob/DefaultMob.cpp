// DefaultMob.cpp : Defines the exported functions for the DLL application.
//

#include "DefaultMob.hpp"

DefaultMob::DefaultMob()
{

}

DefaultMob::~DefaultMob()
{

}

void DefaultMob::init(int id, int x, int y)
{
	Mob::init(id, x, y, 42, 42, -2, 0, DEFAULT, 100, 100);
}

void DefaultMob::toString()
{
	std::cout << "I am the Default Mob !!" << std::endl;
}

extern "C"
{
#ifdef _WIN32
	__declspec(dllexport)
#endif
	Mob *getInstance()
	{
		return new DefaultMob();
	}
}
