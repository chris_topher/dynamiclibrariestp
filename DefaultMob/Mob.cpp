#include "Mob.hpp"

Mob::Mob()
{}

Mob::~Mob()
{}

void	Mob::init(int ID, int x, int y, int sizeX, int sizeY, int speedX, int speedY, int underType, int pts, int bonusChance)
{
	AGameObject::init(ID, MOB, x, y, sizeX, sizeY, speedX, speedY);
	_underType = underType;
	_points = pts;
	_bonusChance = bonusChance;
}