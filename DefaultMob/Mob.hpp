#include "ACreature.hpp"

class Mob : public ACreature
{
public:
	Mob();
	virtual ~Mob();

	virtual void init(int ID, int x, int y, int sizeX, int sizeY, int speedX, int speedY, int underType, int pts, int bonusChance);
	virtual void init(int id, int x, int y) = 0;
	virtual void receiveDmg() {}

private:
	int	_underType;
	int	_points;
	int	_bonusChance;
};
