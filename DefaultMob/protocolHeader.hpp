#pragma once
#include "protocolObjectsFromClient.hpp"
#include "protocolObjectsFromServer.hpp"

enum Direction
{
  UP,
  UPRIGHT,
  RIGHT,
  DOWNRIGHT,
  DOWN,
  DOWNLEFT,
  LEFT,
  UPLEFT
};

enum ObjectType
{
  PLAYER,
  MOB,
  BONUS,
  BULLET
};

enum ObjectSubType
{
  DEFAULT = 0,
  NBMOBTYPES //at the end
  //I don't know :(
};

enum ServerToClientCommand
{
  NEWGAMEOBJECT = 201,
  GAMEOBJECTMOVE,
  GAMEOBJECTDEATH,
  CLIENTDEATH,
  SCORE,
  GAMEEND,
  ROOMSLIST,
  ROOMSELECTRESPONSE,
  GAMELAUNCH
};

enum ClientToServerCommand
{
  MOVE = 101,
  SHOOT,
  LOGIN,
  LOGOUT,
  NEWROOM,
  LAUNCHGAMESESSION,
  ROOMQUIT,
  ROOMSELECT
};

union FromServer
{
  struct NewGameObject newGObject;
	struct GameObjectMovement gObjectMvt;
  struct GameObjectDeath gObjectDeath;
  //struct ClientDeath clDeath;
  struct Score score;
  struct GameEnd gameEnd;
  struct RoomsList roomsList;
  struct RoomSelectResponse roomSelectResponse;
  struct GameLaunch gameLaunch;
};

union FromClient
{
  struct Move move;
  struct Shoot shoot;
  struct Login login;
  struct Logout logout;
  struct NewRoom newRoom;
  struct LaunchGameSession launch;
  struct RoomQuit roomQuit;
  struct RoomSelect roomSelect;
};

struct Packet
{
	FromServer fromServer;
	FromClient fromClient;
};

struct Header
{
  int32_t magic;
  int32_t cmdNb;
  int32_t err;
};

struct SentPacket
{
  Header header;
  Packet packet;
};
