#pragma once
#include <cstdint>

//FromServer objects
//Objects sent by server

//Objects sent by UDP:
struct NewGameObject
{
  int32_t id;
  int32_t type;
  int32_t subType;
  int32_t x;
  int32_t y;
};

struct GameObjectMovement
{
  int32_t id;
  int32_t type;
  int32_t subType;
  int32_t x;
  int32_t y;
};

struct GameObjectDeath
{
  int32_t id;
};

// struct ClientDeath
// {
//
// };

struct Score
{
  int32_t score;
};

struct GameEnd
{
  bool hasWon;
};

//Objects sent by TCP:
struct RoomsList
{
  char roomName[256];
  int32_t   gameType[256];
  char player1[256];
  char player2[256];
  char player3[256];
  char player4[256];
};

struct RoomSelectResponse
{
  bool authorized;
};

struct GameLaunch
{
  //Unknown content for now
};
