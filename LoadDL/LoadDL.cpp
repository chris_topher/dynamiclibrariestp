// LoadDL.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#include "IAnimal.hpp"
#include <iostream>

typedef IAnimal *(*InstancePtr)();

IAnimal *getArmadillo()
{
	HINSTANCE dllArmadillo = NULL;
	InstancePtr inst = NULL;

	dllArmadillo = LoadLibrary(_T("Armadillo.dll"));
	if (dllArmadillo == NULL)
		std::cerr << "Fail while loading library Armadillo" << std::endl;
	inst = reinterpret_cast<InstancePtr>((GetProcAddress(dllArmadillo, "getInstance")));
	if (inst == NULL)
		std::cerr << "Fail while loading getInstance symbol" << std::endl;
	return (inst());
}

int main()
{
	HINSTANCE dllHandle = NULL;
	InstancePtr inst = NULL;

	dllHandle = LoadLibrary(_T("Platypus.dll"));
	if (dllHandle == NULL)
		std::cerr << "Fail while loading library" << std::endl;
	inst = reinterpret_cast<InstancePtr>((GetProcAddress(dllHandle, "getInstance")));

	if (inst == NULL)
		std::cerr << "Fail while loading symbol" << std::endl;
	IAnimal *platypus = inst();
	platypus->cry();
	getArmadillo()->cry();
    return 0;
}

