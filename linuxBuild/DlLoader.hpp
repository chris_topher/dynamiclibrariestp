#pragma once

#include <Windows.h>
#include <tchar.h>
#include <string>
#include <cstring>
#include <stdlib.h>
#include <iostream>
#include "IAnimal.hpp"
#include "IDlLoader.hpp"

class DlLoader : public IDlLoader
{
public:
	wchar_t  *_dllPath;
	HINSTANCE _dllHandle;
	InstancePtr _instPtr;
	DlLoader();
public:
	DlLoader(const std::string &str);
	virtual ~DlLoader();
	virtual IAnimal *getInstance();
};

