#pragma once
#include "IAnimal.hpp"

class IDlLoader
{
public:
	typedef IAnimal *(*InstancePtr)();
	IDlLoader(){}
	virtual ~IDlLoader() {}
	virtual IAnimal *getInstance() = 0;
};

