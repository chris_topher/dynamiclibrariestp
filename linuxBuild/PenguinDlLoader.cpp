#include "PenguinDlLoader.hpp"



PenguinDlLoader::PenguinDlLoader()
{
}


PenguinDlLoader::PenguinDlLoader(const std::string & path)
{
	this->_path = path.c_str();
	this->_handler = dlopen(_path, RTLD_LAZY);
	if (_handler == NULL)
		throw std::runtime_error("PenguinDlLoader::(constructor) : error while loading library");
	this->_inst = reinterpret_cast<InstancePtr>(dlsym(_handler, "getInstance"));
	if (_inst == NULL)
		throw std::runtime_error("PenguinDlLoader::(constructor) : error while loading library");
}

PenguinDlLoader::~PenguinDlLoader()
{
	dlclose(_handler);
}

IAnimal * PenguinDlLoader::getInstance()
{
	return _inst();
}
