#pragma once
#include "IDlLoader.hpp"
#include <string>
#include <dlfcn.h>
#include <stdexcept>

class PenguinDlLoader :
	public IDlLoader
{
protected:
	const char *_path;
	void *_handler;
	InstancePtr _inst;
	PenguinDlLoader();
public:
	PenguinDlLoader(const std::string &str);
	virtual ~PenguinDlLoader();
	virtual IAnimal *getInstance();
};

