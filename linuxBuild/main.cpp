// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "PenguinDlLoader.hpp"
#include <iostream>

int main()
{
	PenguinDlLoader platyDll("platypus/platypus.so");
	PenguinDlLoader armaDll("armadillo/armadillo.so");

	IAnimal *platypus = platyDll.getInstance();
	platypus->cry();
	IAnimal *armadillo = armaDll.getInstance();
	armadillo->cry();
	std::cout << "end of program" << std::endl;
    return 0;
}

