#pragma once
#include "IAnimal.hpp"

class Platypus : public IAnimal
{
public:
	Platypus();
	~Platypus();
	void cry();
};

